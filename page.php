ho<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package MDTheme
 */

get_header(); ?>

	<div id="primary" class="content-area container">
		<main id="main" class="site-main row">
			
			<div class="col">
			<?php
			while ( have_posts() ) : the_post();

				get_template_part( 'template-parts/content', 'page' );
				// If comments are open or we have at least one comment, load up the comment template.
				if ( comments_open() || get_comments_number() ) :
					comments_template();
				endif;
wp_reset_postdata();
wp_reset_query();
			endwhile; // End of the loop.
			?>

<?php
$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;

			$args = array('post_type' => 'activiteit',
						'posts_per_page' => '2',
						
					 'paged' => $paged);                                              
$the_query = new WP_Query( $args );

// The Loop
if ( $the_query->have_posts() ) {
    echo '<div class="row">';
    while ( $the_query->have_posts() ) {
        $the_query->the_post();
        echo '<div class="col-md-4 d-flex align-items-stretch">';
        echo '<div class="card-activiteit">';
        echo '<span>' . get_the_post_thumbnail() . '</span>';
        echo '<div class="card-content">';
        echo '<div>' . get_the_title() . '</div>';
        echo '<div>' . get_field("leeftijd") . '</div>';
        echo '<div>' . get_the_excerpt() . '</div>';
        echo '<a href="' . get_the_permalink() . '?>" class="btn btn-primary">aanmelden</a>';
        echo '</div></div></div>';
    }
    echo '</div>';
    /* Restore original Post Data */
    wp_reset_postdata();

} else {
    // no posts found
}


 ?>

<?php 

$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;

			$args = array('post_type' => 'activiteit',
						'posts_per_page' => '3',
						
					 'paged' => $paged); 
			
// the query
$the_query1 = new WP_Query( $args ); ?>


<?php if ( $the_query1->have_posts() ) : ?>

	<!-- pagination here -->
	
	<div class="row">

	<!-- the loop -->
	<?php while ( $the_query1->have_posts() ) : $the_query1->the_post(); ?>


<div class="col-md-4 d-flex align-items-stretch">


<div class="card text-center">
  <img class="card-img-top" src="<?php if ( has_post_thumbnail() ) {
the_post_thumbnail_url();
} else { ?>
https://cdn.shopify.com/s/files/1/0095/4332/t/30/assets/no-image.svg?4303135395738144762"
<?php } ?>" alt="Card image cap">
<span>
	<?php

$terms = get_the_terms( $post->ID , 'categorieen' );
echo '<ul>';
foreach ( $terms as $term ) {

echo '<li>' . $term->name . '</li>' ; 

}
echo '</ul>';
?></span>
  <div class="card-block">
    <h4 class="card-title"><?php the_title(); ?></h4>
    <p class="card-text"><?php the_field('leeftijd'); ?></p>
    <p class="card-text"><?php the_excerpt(); ?></p>
    <a class="btn btn-primary"><small class="text-muted">aanmelden</small></a>
  </div>
</div>

</div>
	<?php endwhile; ?>
	<!-- end of the loop -->
</div>
	<!-- pagination here -->

	<?php wp_reset_postdata(); ?>

<?php else : ?>
	<p><?php esc_html_e( 'Sorry, no posts matched your criteria.' ); ?></p>
<?php endif; ?>

  
 



			</div>


		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
