<?php
get_header(); ?>
<?php get_template_part( 'template-parts/content', 'callout-2' ); ?>
<div id="primary" class="content-area">
<main id="main" class="site-main container">
<?php
if ( have_posts() ) : ?>
<div class="row">
<?php
while ( have_posts() ) : the_post(); ?>
<?php get_template_part( 'module-parts/content', 'card-blog' ); ?>
<?php endwhile; ?>
</div></div>
<?php the_posts_navigation(); ?>
<?php else :
get_template_part( 'template-parts/content', 'none' );
endif; ?>
</main>
</div>
</main>
</div>
<?php
get_footer();