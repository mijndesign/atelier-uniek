<?php
get_header(); ?>
<?php get_template_part( 'template-parts/content', 'callout-4' ); ?>
<div id="primary" class="content-area container">
<main id="main" class="site-main">
<?php
if ( have_posts() ) : ?>
<div class="page-header row">
<div class="col-12">
<ul class="activiteit-terms">
<li><h4>Filter op: </h4></li>
<li class="alles"><a class="pt-2 pb-2" href="<?php get_template_directory_uri(); ?>/activiteit">Alle Activiteiten</a></li>
<?php
$terms = get_terms('categorieen'); 
$currentterm = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) ); 
foreach ($terms as $term) {
$class = $currentterm->slug == $term->slug ? 'active' : '' ;
echo '<li class="terms '. $class .'"><a class="pt-2 pb-2" href="' . get_term_link( $term ) . '" class="terms ' . $class . '">' . $term->name . '</a></li>'; 
}
?>		
</ul>
</div>
</div>
<div class="row">
<?php
while ( have_posts() ) : the_post(); ?>
<div class="col-md-4 d-flex align-items-stretch box-1">
<?php get_template_part( 'module-parts/content', 'card-activiteit' ); ?>
</div>
<?php endwhile; ?>
</div>
<?php the_posts_navigation(); ?>
<?php else :
get_template_part( 'template-parts/content', 'none' );
endif; ?>
</main>
</div>
<?php
get_footer();