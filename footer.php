</div><!-- #content -->
<footer id="colophon" class="site-footer">
<div class="container-fluid">
<div class="container text-center text-sm-left text-white">
<div class="row">
<div class="col-md-12 mt-5 mb-2">
<h3 class="lead h6 text-white">Atelier <i>Uniek</i></h3>
</div>
</div>
<div class="row pb-5">
<div class="col-sm-4 col-md-4 col-lg-3">
<h3 class="text-white">Adres</h3>
<p class="mb-2"><?php the_field('adres', 'option'); ?></p>
<p class="mb-2"><?php the_field('postcode', 'option'); ?></p>
<p class="mb-2"><?php the_field('plaats', 'option'); ?></p>
</div>
<div class="col-sm-4 col-md-4 col-lg-2">
<h3 class="text-white">Contact</h3>
<ul class="ftu">
<li>
<a href="mailto:info@atelier-uniek.nl">info@atelier-uniek.nl</a></li>
<li><a href="tel:+31624428749">T 0624 4287 49</a></li>
<li><a class="font-weight-bold" href="<?php echo site_url(); ?>/contact">Contactpagina</a></li>
</ul>
</div>
<div class="col-sm-4 col-md-4 col-lg-2">
<h3 class="text-white">Snel Naar</h3>
<ul class="ftu"><li>
<a class="font-weight-bold" href="<?php echo site_url(); ?>/over-ons">Over Ons</a></li>
<li>
<a class="font-weight-bold" href="<?php echo site_url(); ?>/agenda">Agenda</a></li>
<li>
<a class="font-weight-bold" href="<?php echo site_url(); ?>/activiteit">Activiteiten</a></li>
</ul>
</div>
<div class="col-12 col-sm-8 col-md-6 col-lg-5">
<h3 class="text-white">Nieuwsbrief</h3>
<p>Blijf op de hoogte en meld je aan!</p>
<div id="mc_embed_signup">
<form action="https://atelier-uniek.us17.list-manage.com/subscribe/post?u=532d8fa2fcac7ee6ba76debe4&amp;id=0106abfaec" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
<div id="mc_embed_signup_scroll">
<input type="email" value="" name="EMAIL" class="email" id="mce-EMAIL" placeholder="email adres" required></div>
<!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
<div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_532d8fa2fcac7ee6ba76debe4_0106abfaec" tabindex="-1" value=""></div>
<div class="clear"><input type="submit" value="Aanmelden" name="subscribe" id="mc-embedded-subscribe" class="btn btn-secondary bg-success border-0"></div>
</div>
</form>
</div>
</div>
</div>
</div>
</div>
<div class="footer-copyright container-fluid bg-yellow">
<div class="row p-3">
<div class="col-sm-12 text-center text-muted h6">
© <?php echo date('Y'); ?> <?php bloginfo('name'); ?>.
</div>
</div>
</div>
</footer><!-- #colophon -->
</div><!-- #page -->
<?php wp_footer(); ?>
<script src="https://unpkg.com/scrollreveal/dist/scrollreveal.min.js"></script>
<script>window.sr = ScrollReveal();
if (sr.isSupported()) {
document.documentElement.classList.add('sr');
}
sr.reveal('.box-1', { duration: 1000,delay: 0,scale: 1,viewFactor: 0.1 }, 200);
sr.reveal('.box-2', { duration: 1000,delay: 100,scale: 1}, 80);
sr.reveal('.box-3', { duration: 1000,delay: 200,scale: 1}, 80);
sr.reveal('.box-4', { duration: 1000,delay: 0,scale: 1}, 80);
sr.reveal('.box-5', { duration: 1000,delay: 100,scale: 1}, 80);
sr.reveal('.box-6', { duration: 1000,delay: 200,scale: 1}, 80);
</script>
</body>
</html>