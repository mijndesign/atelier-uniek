<?php
/* Template Name: Contact */
get_header(); ?>
<?php get_template_part( 'template-parts/content', 'callout-2' ); ?>
<div id="primary" class="content-area container">
<div class="row">
<main id="main" class="site-main col-sm-12 col-md-8 ">
<?php get_template_part( 'template-parts/contact/content', 'row-2' ); ?>
<?php get_template_part( 'module-parts/team', 'user' ); ?>
<?php get_template_part( 'template-parts/contact/content', 'row-3' ); ?>
<?php get_template_part( 'template-parts/contact/content', 'row-4' ); ?>
<?php get_template_part( 'template-parts/contact/content', 'row-5' ); ?>
</main>
<div class="col-sm-12 col-md-4">
<div class="card p-4 border-0 z-depth-1">
<?php get_template_part( 'module-parts/content', 'recentpost' ); ?>
</div>
</div>
</div>
</div>
<?php
get_footer();
