<?php
/* Template Name: Home */
get_header(); ?>
<?php get_template_part( 'template-parts/content', 'callout' ); ?>
<div id="primary" class="content-area container">
<main id="main" class="site-main">
<?php get_template_part( 'template-parts/home/content', 'row-2' ); ?>
<?php get_template_part( 'template-parts/home/content', 'row-3' ); ?>
<?php get_template_part( 'template-parts/home/content', 'row-4' ); ?>
</main>
</div>
<?php
get_footer();