<div class="col-12 col-sm-12 col-md-6 col-lg-4 d-flex align-items-stretch box-5">
<div class="card z-depth-1 border-0 text-left hac">
<a href="<?php the_permalink(); ?>"><img class="card-img-top" src="<?php if ( has_post_thumbnail() ) { the_post_thumbnail_url(); } else { ?><?php echo get_template_directory_uri(); ?>/assets/img/logo.svg <?php } ?>" alt="Atelier Uniek Blogs"></a>
<span class="term">
<?php
$terms = get_the_terms( $post->ID , 'category' );
echo '<ul class="pt-2 pb-2 pl-4 pr-4 card border-0 text-white">';
foreach ( $terms as $term ) {
echo '<li>' . $term->name . '</li>' ; }
echo '</ul>'; 
?>
</span>
<div class="card-block">
<h2 class="card-title p-2 trim mt-2 mb-0"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
<p class="card-text p-2"><?php 
echo wp_trim_words( get_the_content(), 25 ); ?></p>
</div>
<div class="card-footer blg ml-0 mr-0 row">
<div class="col-6 col-sm-6 col-md-12 col-lg-6 text-left text-md-center text-lg-left pl-1 pr-1">
<small class="text-muted"><?php the_date(); ?></small>
</div>
<div class="col-6 col-6 col-sm-6 col-md-12 col-lg-6 text-right text-md-center text-lg-right pl-1 pr-1">
<a href="<?php the_permalink(); ?>">lees meer</a>
</div>
</div>
</div>
</div>