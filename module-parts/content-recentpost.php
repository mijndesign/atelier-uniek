<ul class="recent-post pl-0 ml-0 mb-0 ">
<h3 class="mb-3">Recente berichten</h3>
<?php 
$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
$args = array(
'posts_per_page' => '7',
'post_status' => 'publish',
'paged' => $paged); 
$the_query1 = new WP_Query( $args ); ?>
<?php if ( $the_query1->have_posts() ) : ?>
<?php while ( $the_query1->have_posts() ) : $the_query1->the_post(); ?>
<li class="mb-3">
<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
</li>
<?php endwhile; ?>
<?php wp_reset_postdata(); ?>
<?php else : ?>
<?php endif; ?>		
</ul>