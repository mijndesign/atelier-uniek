<div class="card hac border-0 text-center pb-3 z-depth-1">
<a href="<?php the_permalink(); ?>">
<img class="card-img-top mb-3" src="<?php if ( has_post_thumbnail() ) { the_post_thumbnail_url(); }else { ?> https://cdn.shopify.com/s/files/1/0095/4332/t/30/assets/no-image.svg?4303135395738144762" <?php } ?>" alt="Atelier Uniek activiteiten">
</a>
<span class="term">
<?php
$terms = get_the_terms( $post->ID , 'categorieen' );
echo '<ul class="pt-2 pb-2 pl-4 pr-4 card border-0 text-white">';
foreach ( $terms as $term ) {
echo '<li>' . $term->name . '</li>' ; }
echo '</ul>'; 
?>
</span>
<span class="d-none <?php the_field('vol'); ?> available w-100 position-absolute text-center text-white justify-content-center">
<p class="mb-0 font-weight-normal"><?php the_field('text_vol'); ?></p>
</span>
<div class="card-block">
<h3 class="card-title mb-1"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
<p class="card-text text-muted"><?php the_field('leeftijd'); ?></p>
<p class="card-text pl-1 pr-1"><?php echo wp_trim_words( get_the_content(), 15 ); ?></p>
<a class="btn btn-2" href="<?php the_permalink(); ?>">Bekijk activiteit</a>
</div>
</div>