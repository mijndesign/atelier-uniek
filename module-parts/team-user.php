<div class="card border-0 z-depth-1 p-4">
<?php
while ( have_posts() ) : the_post();
get_template_part( 'template-parts/content', 'page' );
if ( comments_open() || get_comments_number() ) :
comments_template();
endif;
wp_reset_postdata();
wp_reset_query();
endwhile; 
?>
<?php 
$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
$args = array(
'post_type' => 'team',
'paged' => $paged); 
$the_query1 = new WP_Query( $args ); ?>
<?php if ( $the_query1->have_posts() ) : ?>
<?php while ( $the_query1->have_posts() ) : $the_query1->the_post(); ?>
<ul class="mt-2 team">
<li class="h4 font-weight-normal"><?php the_field('naam'); ?></li>
<li class="mt-2"><a href="mailto:<?php the_field('e-mail'); ?>"><img class="mr-2" src="<?php echo get_template_directory_uri(); ?>/assets/img/close-envelope.svg" height="23" width="23"><?php the_field('e-mail'); ?></a></li>
<li class="mt-2"><a href="tel:<?php the_field('telefoon'); ?>"><img class="mr-2" src="<?php echo get_template_directory_uri(); ?>/assets/img/phone.svg" height="20" width="20"><?php the_field('telefoon'); ?></a></li>	
</ul>
<?php endwhile; ?>
</div>
<?php wp_reset_postdata(); ?>
<?php else : ?>
<?php endif; ?>