<?php
get_header(); ?>
<?php get_template_part( 'template-parts/content', 'callout' ); ?>
<div id="primary" class="content-area container">
<main id="main" class="site-main">
<?php
if ( have_posts() ) : ?>
<div class="row">
<?php
while ( have_posts() ) : the_post(); ?>
<div class="col-12 col-sm-12 col-md-4">
<div class="card border-0 text-center pb-3 z-depth-1">
<a href="<?php the_permalink(); ?>">
<img class="card-img-top mb-3" src="<?php if ( has_post_thumbnail() ) { the_post_thumbnail_url(); }else { ?> https://cdn.shopify.com/s/files/1/0095/4332/t/30/assets/no-image.svg?4303135395738144762" <?php } ?>" alt="Atelier Uniek activiteiten">
</a>
<h3 class="card-title">
<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
</div>
</div>
<?php endwhile; ?>
</div>
<?php the_posts_navigation(); ?>
<?php else :
get_template_part( 'template-parts/content', 'none' );
endif; ?>
</main>
</div>
<?php
get_footer();