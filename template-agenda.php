<?php
/* Template Name: Agenda */
get_header(); ?>
<?php get_template_part( 'template-parts/content', 'callout-2' ); ?>
<div id="primary" class="content-area container">
<main id="main" class="site-main">
<?php get_template_part( 'template-parts/agenda/content', 'row-2' ); ?>
<?php get_template_part( 'template-parts/agenda/content', 'row-3' ); ?>
<?php get_template_part( 'template-parts/agenda/content', 'row-4' ); ?>
<?php get_template_part( 'template-parts/agenda/content', 'row-5' ); ?>
</main>
</div>
<?php
get_footer();