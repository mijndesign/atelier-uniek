<?php
$loop = new WP_Query( array( 'post_type' => 'agenda', 'order' => 'ASC' ) );
if ( $loop->have_posts() ) :
while ( $loop->have_posts() ) : $loop->the_post(); ?>
<div class="row">
<div class="col-sm-12 box-1">   
<div class="card card-agenda p-4 border-0 z-depth-1"> 
<h2 class="mb-3 text-center text-md-left"><?php the_title(); ?> </h2>
<?php
if( have_rows('herhaal_agenda') ): 
while ( have_rows('herhaal_agenda') ) : the_row(); ?>
<ul class="row agenda text-center text-md-left p-0 pb-3 m-0 mb-2 mt-1">     
<li class="col-sm-12 col-md-3 col-xl-2 order-2 order-md-1 mt-1 pb-0">
<h4 class="m-0 p-0 mb-1"> 
<?php echo the_sub_field('datum_agenda'); ?> 
</h4>
<p class="text-muted naam mb-0"> 
<?php the_sub_field('tijd_agenda'); ?> 
</p>
</li>
<li class="col-sm-12 col-md-9 col-xl-10 order-1 order-md-2 mt-1 pb-0">
<?php 
$post_id = get_sub_field('activiteit_agenda_a', false, false);
if( $post_id ): ?>
<a class="p-0 m-0" href="<?php echo get_the_permalink($post_id); ?>"><h3 class="m-0 p-0 mb-1 h6"><?php echo get_the_title($post_id); ?></h3> <small class="text-muted"> <?php the_sub_field('extra_agenda'); ?></small></a>
<?php endif; ?>
<p class="text-muted naam mb-0">Door: <?php the_sub_field('gegeven_agenda'); ?></p>
</li>
</ul>
<?php endwhile; ?>
<?php
else :
endif;
?>
</div>
</div>
</div>
<?php endwhile; ?>
<?php endif; ?>
<?php wp_reset_postdata();
?>
