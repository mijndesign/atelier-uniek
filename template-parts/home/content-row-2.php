<?php 
$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
$args = array(
'post_type' => 'activiteit',
'posts_per_page' => '1',
'orderby' => 'rand',
'paged' => $paged,
'tax_query' => array(
array(
'taxonomy' => 'beschikbaaheid',
'field'    => 'slug',
'terms'    => 'uitgelicht',
),
),
); 
$the_query1 = new WP_Query( $args ); ?>
<?php if ( $the_query1->have_posts() ) : ?>
<?php while ( $the_query1->have_posts() ) : $the_query1->the_post(); ?>
<div class="row spc">
<div class="col-12 text-center">
<h4>Uitgelichte activiteiten</h4>
<span class="chev animated infinite fadeInDown">&#x2304;</span>
</div>
</div>
<div class="row align-items-center hr1 mb-5 mt-2 z-depth-1">
<div id="chev" class="col-12 col-sm-12 col-lg-8  order-2 order-xl-1 text-center text-lg-left sap">
<h2 class="card-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
<small class="text-muted"><?php the_field('leeftijd'); ?></small>
<h5 class="mb-0 mt-3"><small class="">Startdatum: </small><?php the_field('datum') ?></h5>
<p class="p-0 mb-4 time"><small class="">Tijd: </small><?php the_field('tijd'); ?></p>
<a class="btn btn-3 mt-2 mb-2" href="<?php the_permalink(); ?>">Bekijk deze activiteit</a>
<a class="btn btn-4 mt-2 mb-2 ml-4 mr-4" href="<?php echo site_url(); ?>/activiteit">Bekijk alle activiteiten</a>
</div>
<div class="col-lg-4 p-0 order-1 order-xl-2">
<a href="<?php the_permalink(); ?>"><img class="card-img-top" src="<?php if ( has_post_thumbnail() ) {
the_post_thumbnail_url();
} else { ?>
https://cdn.shopify.com/s/files/1/0095/4332/t/30/assets/no-image.svg?4303135395738144762"
<?php } ?>" alt="uitgelicht Atelier Uniek" height="200"></a></div>
</div>
<?php endwhile; ?>
<?php wp_reset_postdata(); ?>
<?php else : ?>
<?php endif; ?>