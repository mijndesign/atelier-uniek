<div class="row mb-3">
<div class="col-sm-12 pl-1>">
<h2 class="title-secondary">Populaire activiteiten</h2>
</div>
</div>
<div id="test" class="row">
<?php 
$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
$args = array(
'post_type' => 'activiteit',
'posts_per_page' => '1',
'paged' => $paged); 
$the_query1 = new WP_Query( $args ); ?>
<?php if ( $the_query1->have_posts() ) : ?>
<?php while ( $the_query1->have_posts() ) : $the_query1->the_post(); ?>
<div class="col-md-4 d-flex align-items-stretch box-1">
<?php get_template_part( 'module-parts/content', 'card-activiteit' ); ?>
</div>
<?php endwhile; ?>
<?php wp_reset_postdata(); ?>
<?php else : ?>
<?php endif; ?>
<?php 
$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
$args = array(
'post_type' => 'activiteit',
'posts_per_page' => '1',
'offset' => '1',
'paged' => $paged); 
$the_query1 = new WP_Query( $args ); ?>
<?php if ( $the_query1->have_posts() ) : ?>
<?php while ( $the_query1->have_posts() ) : $the_query1->the_post(); ?>
<div class="col-md-4 d-flex align-items-stretch box-2">
<?php get_template_part( 'module-parts/content', 'card-activiteit' ); ?>
</div>
<?php endwhile; ?>
<?php wp_reset_postdata(); ?>
<?php else : ?>
<?php endif; ?>
<?php 
$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
$args = array(
'post_type' => 'activiteit',
'posts_per_page' => '1',
'offset' => '2',
'paged' => $paged); 
$the_query1 = new WP_Query( $args ); ?>
<?php if ( $the_query1->have_posts() ) : ?>
<?php while ( $the_query1->have_posts() ) : $the_query1->the_post(); ?>
<div class="col-md-4 d-flex align-items-stretch box-3">
<?php get_template_part( 'module-parts/content', 'card-activiteit' ); ?>
</div>
<?php endwhile; ?>
<?php wp_reset_postdata(); ?>
<?php else : ?>
<?php endif; ?>
</div>
<div class="row mb-1">
<div class="col-12 text-center text-sm-right">
<p class="bbb"><a href="<?php echo site_url(); ?>/activiteit">Bekijk alle activiteiten &#xbb;</a></p>
</div>
</div>