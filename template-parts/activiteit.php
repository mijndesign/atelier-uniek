<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
<div class="row">
<div class="col-md-8">
<div class="entry-content-activiteit card sap p-4 border-0 z-depth-1">
<span class="d-none <?php the_field('vol'); ?> available single-available w-100 position-absolute text-center text-white justify-content-center">
<p class="mb-0 font-weight-normal"><?php the_field('text_vol'); ?></p>
</span>
<h2 class="mb-0"><?php the_title(); ?></h2>
<small class="text-muted">Leeftijd: <?php the_field('leeftijd') ?></small>
<h4 class="mb-0 mt-3"><small class="">Startdatum: </small><?php the_field('datum') ?></h4>
<small class="mb-2 time"><small class="">Tijd: </small><?php the_field('tijd') ?></small>
<p class="mb-3"><?php the_field('tekst') ?></p>
<p class="price mb-2 <?php the_field('aanmelden_uitschakelen') ?>">€<?php the_field('price') ?></p>
<a class="scroll btn btn-primary <?php the_field('aanmelden_uitschakelen') ?>" href="#pay">Aanmelden</a>
</div>
<div class="entry-content-activiteit card p-4 z-depth-1 border-0">
<h2 class="mb-0"><?php the_title(); ?></h2>
<small class="text-muted mb-3"> (<?php the_field('leeftijd') ?>)</small>
<?php the_content(); ?>
</div>
<div id="pay" class="card p-4 border-0 sap z-depth-1 <?php the_field('aanmelden_uitschakelen') ?>">
<h2 class="mb-0"><?php the_title(); ?></h2>
<small class="text-muted"><?php the_field('leeftijd') ?></small>
<h4 class="mb-0 mt-3"><?php the_field('datum') ?></h4>
<small class="mb-2"><?php the_field('tijd') ?></small>
<h2 class="mt-0">€ <?php the_field('price') ?></h2>
<p><?php the_field('aanmeld_notitie') ?></p>
<div class="col-sm-12 col-md-8 col-xl-6 m-0 p-0">
<?php echo do_shortcode( '[contact-form-7 id="9" title="Blank"]' ); ?></div>
</div>
</div>
<div class="col-md-4"><div class="card border-0 z-depth-1 p-4">
<h3 class="mb-3">Andere activiteiten</h3>
<ul class="recent-post pl-0 ml-0 mb-0">
<?php
$recent_posts = wp_get_recent_posts(array('post_type'=>'activiteit'));
foreach( $recent_posts as $recent ){
echo '<li class="mb-2"><a href="' . get_permalink($recent["ID"]) . '" title="Look '.esc_attr($recent["post_title"]).'" >' .   $recent["post_title"].'</a> </li> ';
}
?>
</ul>
</div>
<div class="card p-4 border-0 z-depth-1">
<h2 class="mb-3">Deel bericht</h2>
<ul class="ftu mb-0">
<li class="float-left">
<a href="https://www.facebook.com/sharer/sharer.php?u=<?php the_permalink(); ?>" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/facebook.svg" height="30px" width="30"></a>
</li>
<li class="float-left pl-3">
<a href="https://twitter.com/home?status=<?php the_permalink(); ?>" target="_blank"><img  src="<?php echo get_template_directory_uri(); ?>/assets/img/twitter.svg" height="30px" width="30"></a>
</li>
</ul>
</div>
</div>
</div>
<footer class="entry-footer">
<?php mdtheme_entry_footer(); ?>
</footer>
</article><!-- #post-<?php the_ID(); ?> -->
