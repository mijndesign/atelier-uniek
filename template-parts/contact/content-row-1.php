<header class="entry-header-page">
<div class="row pt-5 pb-5">
<div class="col-sm-12 col-md-6 col-xl-8">
<?php the_title( '<h1 class="lead entry-title">', '</h1>' ); ?>
<div class="header-content">
<p>Creatieve bezigheden voor ieder leeftijd. Van workshops tot cursussen. Bij Atelier Uniek is het allemaal mogelijk. Altijd na schooltijd en voor een betaalbaar bedrag.<br /> Bekijk hieronder onze contact</p>
</div>
</div>
</div>
</header>