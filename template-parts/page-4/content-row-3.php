<?php 
$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
$args = array('post_type' => 'team',
); 
$the_query1 = new WP_Query( $args ); ?>
<?php if ( $the_query1->have_posts() ) : ?>
<?php while ( $the_query1->have_posts() ) : $the_query1->the_post(); ?>
<div class="card border-0 z-depth-1 box-2" style="overflow: hidden;">
<div class="row ">
<div class="col-md-5 p-0">
<img class="card-img-top" src="<?php if ( has_post_thumbnail() ) {
the_post_thumbnail_url();
} else { ?>
https://cdn.shopify.com/s/files/1/0095/4332/t/30/assets/no-image.svg?4303135395738144762"
<?php } ?>" alt="uitgelicht Atelier Uniek" height="200px"></div>
<div class="col-md-7 p-3 d-block align-items-stretch">
<h2><?php the_title(); ?></h2>
<p><?php the_content(); ?></p>
</div>
</div>
</div>
<?php endwhile; ?>
<?php wp_reset_postdata(); ?>
<?php else : ?>
<p><?php esc_html_e( 'Sorry, no posts matched your criteria.' ); ?></p>
<?php endif; ?>