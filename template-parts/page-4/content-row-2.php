<div class="card p-4 border-0 z-depth-1 box-1">
<?php
while ( have_posts() ) : the_post();
get_template_part( 'template-parts/content', 'page' );
if ( comments_open() || get_comments_number() ) :
comments_template();
endif;
wp_reset_postdata();
wp_reset_query();
endwhile; 
?>
</div>