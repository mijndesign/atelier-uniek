<div class="container-fluid callout d-flex align-items-center cal-3">
<div class="container ">
<div class="row pt-0 pb-5">
<div class="col-sm-12 col-md-8 col-xl-8">
<?php the_title( '<h1 class="lead entry-title">', '</h1>' ); ?>
<div class="header-content">
<p>Atelier Uniek organiseert activiteiten, cursussen en workshops die de creativiteit stimuleren.
</p>
</div>
</div>
</div>
</div>
</div>
