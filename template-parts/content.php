<?php
/**
* Template part for displaying posts
*
* @link https://developer.wordpress.org/themes/basics/template-hierarchy/
*
* @package MDTheme
*/
?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
<header class="entry-header-content">
<?php
if ( is_singular() ) :
the_title( '<h1 class="entry-title mb-0 mt-0">', '</h1>' );
else :
the_title( '<h2 class="entry-title mb-0 mt-0"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
endif;
if ( 'post' === get_post_type() ) : ?>
<small class="entry-meta font-weight-normal text-muted">
<?php //mdtheme_posted_on(); ?>
<?php the_date(); ?>
</small>
<?php
endif; ?>
</header>
<div class="entry-content">
<img class="w-50 mb-3" src="<?php if ( has_post_thumbnail() ) {
the_post_thumbnail_url();
} else { ?>
<?php echo get_template_directory_uri(); ?>/assets/img/logo.svg"
<?php } ?>" alt="Atelier Uniek Blogs">
<?php
the_content( sprintf(
wp_kses(
/* translators: %s: Name of current post. Only visible to screen readers */
__( 'Continue reading<span class="screen-reader-text"> "%s"</span>', 'mdtheme' ),
array(
'span' => array(
'class' => array(),
),
)
),
get_the_title()
) );
wp_link_pages( array(
'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'mdtheme' ),
'after'  => '</div>',
) );
?>
</div><!-- .entry-content -->
<footer class="entry-footer">
<?php //mdtheme_entry_footer(); ?>
</footer><!-- .entry-footer -->
</article><!-- #post-<?php the_ID(); ?> -->