<?php
get_header(); ?>
<?php get_template_part( 'template-parts/content', 'callout-2' ); ?>
<div id="primary" class="content-area container">
<main id="main" class="site-main">
<?php
while ( have_posts() ) : the_post();
get_template_part( 'template-parts/activiteit', get_post_type() ); ?>
<?php //the_post_navigation(); ?>
<?php
if ( comments_open() || get_comments_number() ) :
comments_template();
endif;
endwhile;
?>
</main>
</div>
<?php
get_footer();