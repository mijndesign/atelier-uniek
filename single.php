<?php
get_header(); ?>
<?php get_template_part( 'template-parts/content', 'callout-2' ); ?>
<div id="primary" class="content-area container">
<main id="main" class="site-main row">
<div class="col-12 col-sm-12 col-md-8">
<div class="card  pl-4 pr-4 pt-4 pb-0 border-0 z-depth-1">
<?php
while ( have_posts() ) : the_post();
get_template_part( 'template-parts/content', get_post_type() );
the_post_navigation(array(
'prev_text'                  => __( 'vorige bericht' ),
'next_text'                  => __( 'volgende bericht' ),
));
endwhile; 
?>	
</div>
</div>
<div class="col-sm-12 col-md-4">
<div class="card p-4 border-0 z-depth-1">
<?php get_template_part( 'module-parts/content', 'recentpost' ); ?>
</div>
<div class="card p-4 border-0 z-depth-1">
<h2 class="mb-3">Deel bericht</h2>
<ul class="ftu mb-0">
<li class="float-left">
<a href="https://www.facebook.com/sharer/sharer.php?u=<?php the_permalink(); ?>" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/facebook.svg" height="30px" width="30"></a>
</li>
<li class="float-left pl-3">
<a href="https://twitter.com/home?status=<?php the_permalink(); ?>" target="_blank"><img  src="<?php echo get_template_directory_uri(); ?>/assets/img/twitter.svg" height="30px" width="30"></a>
</li>
</ul>
</div>
</div>
</main>
</div>
<?php
get_footer();