<?php
get_header(); ?>
<?php get_template_part( 'template-parts/content', 'callout-4' ); ?>
<div id="primary" class="content-area container">
<main id="main" class="site-main">
<?php
if ( have_posts() ) : ?>
<header class="page-header row">
<div class="col-12">
<ul class="activiteit-terms">
<li><h4>Filter op: </h4></li>
<li class="alles"><a href="<?php get_template_directory_uri(); ?>/activiteit">Alle Activiteiten</a></li>
<?php
$terms = get_terms('categorieen'); 
$currentterm = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) ); 
foreach ($terms as $term) {
$class = $currentterm->slug == $term->slug ? 'active' : '' ;
echo '<li class="terms '. $class .'"><a href="' . get_term_link( $term ) . '" class="terms ' . $class . '">' . $term->name . '</a></li>'; 
}
?>		</ul>
</div>
</header>
<div class="row">
<?php
while ( have_posts() ) : the_post(); ?>
<div class="col-md-4 d-flex align-items-stretch box-1">
<div class="card text-center pb-3 border-0 z-depth-1 hac">
<img class="card-img-top mb-3" src="<?php if ( has_post_thumbnail() ) { the_post_thumbnail_url(); }else { ?>
https://cdn.shopify.com/s/files/1/0095/4332/t/30/assets/no-image.svg?4303135395738144762"
<?php } ?>" alt="atelier uniek activiteiten">
<span class="term">
<?php
$terms = get_the_terms( $post->ID , 'categorieen' );
echo '<ul class="pt-2 pb-2 pl-4 pr-4 card border-0 text-white">';
foreach ( $terms as $term ) {
echo '<li>' . $term->name . '</li>' ; }
echo '</ul>'; 
?>
</span>
<div class="card-block">
<h3 class="card-title mb-1"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
<p class="card-text text-muted">
<?php the_field('leeftijd'); ?>
</p>
<p class="card-text">
<?php echo wp_trim_words( get_the_content(), 15 ); ?>
</p>
<a class="btn btn-2" href="<?php the_permalink(); ?>">
Bekijk activiteit
</a>
</div>
</div>
</div>
<?php endwhile; ?>
</div></div>
<?php the_posts_navigation(); ?>
<?php else :
get_template_part( 'template-parts/content', 'none' );
endif; ?>
</main>
</div>
<?php
get_footer();