<?php

function cpt_visie() {

	/**
	 * Post Type: Visie.
	 */

	$labels = array(
		"name" => __( "Visie", "mdtheme" ),
		"singular_name" => __( "Visie", "mdtheme" ),
		"menu_name" => __( "Visie", "mdtheme" ),
		"add_new" => __( "Visie toevoegen", "mdtheme" ),
		"edit_item" => __( "Bewerk visie", "mdtheme" ),
	);

	$args = array(
		"label" => __( "Visie", "mdtheme" ),
		"labels" => $labels,
		"description" => "",
		"public" => false,
		"publicly_queryable" => false,
		"show_ui" => true,
		"show_in_rest" => false,
		"rest_base" => "",
		"has_archive" => false,
		"show_in_menu" => true,
		"exclude_from_search" => true,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"rewrite" => array( "slug" => "visie", "with_front" => true ),
		"query_var" => false,
		"supports" => array( "title", "editor", "thumbnail" ),
	);

	register_post_type( "visie", $args );
}

add_action( 'init', 'cpt_visie' );

