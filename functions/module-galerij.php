<?php
function cptui_register_my_cpts_galerij() {

	/**
	 * Post Type: galerij.
	 */

	$labels = array(
		"name" => __( "galerij", "mdtheme" ),
		"singular_name" => __( "galerij", "mdtheme" ),
		"all_items" => __( "galerij", "mdtheme" ),
		'add_new' => __( 'Nieuw galerij', 'twentythirteen' ),
		'add_new_item' => __( 'Nieuw galerij', 'twentythirteen' ),
	);

	$args = array(
		"label" => __( "galerij", "mdtheme" ),
		"labels" => $labels,
		"description" => "",
		"public" => false,
		"publicly_queryable" => flase,
		"show_ui" => true,
		"show_in_rest" => false,
		"rest_base" => "",
		"has_archive" => true,
		"show_in_menu" => true,
		"exclude_from_search" => false,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"rewrite" => array( "slug" => "galerij", "with_front" => true ),
		"query_var" => false,
		"supports" => array( "title", "editor", "thumbnail" ),
	);

	register_post_type( "galerij", $args );
}

add_action( 'init', 'cptui_register_my_cpts_galerij' );
