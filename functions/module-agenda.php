<?php

function cpt_agenda() {

	/**
	 * Post Type: agenda.
	 */

	$labels = array(
		"name" => __( "agenda", "mdtheme" ),
		"singular_name" => __( "agenda", "mdtheme" ),
		"menu_name" => __( "agenda", "mdtheme" ),
		"add_new" => __( "agenda toevoegen", "mdtheme" ),
		"edit_item" => __( "Bewerk agenda", "mdtheme" ),
	);

	$args = array(
		"label" => __( "agenda", "mdtheme" ),
		"labels" => $labels,
		"description" => "",
		"public" => false,
		"publicly_queryable" => false,
		"show_ui" => true,
		"show_in_rest" => false,
		"rest_base" => "",
		"has_archive" => false,
		"show_in_menu" => true,
		"exclude_from_search" => true,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"rewrite" => array( "slug" => "agenda", "with_front" => true ),
		"query_var" => false,
		"supports" => array( "title", "editor", "thumbnail" ),
	);

	register_post_type( "agenda", $args );
}

add_action( 'init', 'cpt_agenda' );

