<?php
function cptui_register_my_cpts_activiteit() {

	/**
	 * Post Type: Activiteiten .
	 */

	$labels = array(
		"name" => __( "Activiteiten ", "mdtheme" ),
		"singular_name" => __( "activiteit", "mdtheme" ),
		"add_new" => __( "Activiteit toevoegen", "mdtheme" ),
		"view_item" => __( "Bekijken ", "mdtheme" ),
		"view_items" => __( "Bekijken ", "mdtheme" ),
	);

	$args = array(
		"label" => __( "Activiteiten ", "mdtheme" ),
		"labels" => $labels,
		"description" => "",
		"public" => true,
		"publicly_queryable" => true,
		"show_ui" => true,
		"show_in_rest" => false,
		"rest_base" => "",
		"has_archive" => true,
		"show_in_menu" => true,
		"exclude_from_search" => false,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"rewrite" => array( "slug" => "activiteit", "with_front" => true ),
		"query_var" => true,
		"supports" => array( "title", "editor", "thumbnail", "excerpt" ),
		"taxonomies" => array( "naam", "beschikbaaheid", "categorieen" ),
	);

	register_post_type( "activiteit", $args );
}

add_action( 'init', 'cptui_register_my_cpts_activiteit' );

function cptui_register_my_taxes_naam() {

	/**
	 * Taxonomy: Namen.
	 */

	$labels = array(
		"name" => __( "Namen", "mdtheme" ),
		"singular_name" => __( "Naam", "mdtheme" ),
	);

	$args = array(
		"label" => __( "Namen", "mdtheme" ),
		"labels" => $labels,
		"public" => true,
		"hierarchical" => true,
		"label" => "Namen",
		"show_ui" => true,
		"show_in_menu" => true,
		"show_in_nav_menus" => true,
		"query_var" => true,
		"rewrite" => array( 'slug' => 'naam', 'with_front' => true, ),
		"show_admin_column" => false,
		"show_in_rest" => false,
		"rest_base" => "",
		"show_in_quick_edit" => false,
	);
	register_taxonomy( "naam", array( "activiteit" ), $args );
}

add_action( 'init', 'cptui_register_my_taxes_naam' );

function cptui_register_my_taxes_beschikbaaheid() {

	/**
	 * Taxonomy: Beschikbaarheid.
	 */

	$labels = array(
		"name" => __( "Beschikbaar", "mdtheme" ),
		"singular_name" => __( "Beschikbaar", "mdtheme" ),
	);

	$args = array(
		"label" => __( "Beschikbaarheid", "mdtheme" ),
		"labels" => $labels,
		"public" => true,
		"hierarchical" => true,
		"label" => "Beschikbaarheid",
		"show_ui" => true,
		"show_in_menu" => true,
		"show_in_nav_menus" => true,
		"query_var" => true,
		"rewrite" => array( 'slug' => 'beschikbaaheid', 'with_front' => true, ),
		"show_admin_column" => false,
		"show_in_rest" => false,
		"rest_base" => "",
		"show_in_quick_edit" => true,
	);
	register_taxonomy( "beschikbaaheid", array( "activiteit" ), $args );
}

add_action( 'init', 'cptui_register_my_taxes_beschikbaaheid' );

function cptui_register_my_taxes_categorieen() {

	/**
	 * Taxonomy: Categorieën.
	 */

	$labels = array(
		"name" => __( "Categorieën", "mdtheme" ),
		"singular_name" => __( "Categorie", "mdtheme" ),
	);

	

	$args = array(
		"label" => __( "Categorieën", "mdtheme" ),
		"labels" => $labels,
		"public" => true,
		"hierarchical" => true,
		"label" => "Categorieën",
		"show_ui" => true,
		"show_in_menu" => true,
		"show_in_nav_menus" => true,
		"query_var" => true,
		"rewrite" => array( 'slug' => 'categorieen', 'with_front' => true, ),
		"show_admin_column" => false,
		"show_in_rest" => false,
		"rest_base" => "",
		"show_in_quick_edit" => false,
	);
	register_taxonomy( "categorieen", array( "activiteit" ), $args );
}

add_action( 'init', 'cptui_register_my_taxes_categorieen' );





