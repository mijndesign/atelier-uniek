<?php

function mdtheme_scripts() {

    //wp_enqueue_style( 'mdtheme-style', get_stylesheet_uri() );

    wp_enqueue_style( 'main-stylesheet', get_template_directory_uri() . '/assets/styles/css/main.css', array(), '1.2', 'all' );

    wp_enqueue_style( 'google-fonts', 'https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,600', false ); 


      wp_enqueue_style( 'font-allura', 'https://fonts.googleapis.com/css?family=Allura', false );

    wp_deregister_script( 'jquery' );
    
    wp_enqueue_script( 'jquery', 'https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js', array(), '3.2.1', true );

    wp_enqueue_script( 'mdtheme-skip-link-focus-fix', get_template_directory_uri() . '/assets/js/skip-link-focus-fix.js', array(), '20151215', true );

 


    wp_enqueue_script( 'popper', 'https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js', array(), '1.12.9', true );

    wp_enqueue_script( 'bootstrap-js', get_template_directory_uri() . '/vendor/bootstrap-4-dev/dist/js/bootstrap.min.js', array(), '201512151', true );

     wp_enqueue_script( 'side-nav', get_template_directory_uri() . '/vendor/pushy/js/pushy.min.js', array(), '201512151', true );

     wp_enqueue_script( 'custom-scripts', get_template_directory_uri() . '/assets/js/custom-scripts.js', array(), '1.2.0', true );
     wp_enqueue_script( 'flex-slider', get_template_directory_uri() . '/assets/js/jquery.flexslider.js', array(), '1.0.2', true );

      wp_enqueue_script( 'custom-flex', get_template_directory_uri() . '/assets/js/flex-slider.js', array(), '1.0.2', true );

     //wp_enqueue_script( 'wow', get_template_directory_uri() . '/assets/js/wow.min.js', array(), '1.0.0', true );

    if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
        wp_enqueue_script( 'comment-reply' );
    }
}
add_action( 'wp_enqueue_scripts', 'mdtheme_scripts' );