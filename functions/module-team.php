<?php
function cptui_register_my_cpts_team() {

	/**
	 * Post Type: Team.
	 */

	$labels = array(
		"name" => __( "Team", "twentyseventeen" ),
		"singular_name" => __( "Team", "twentyseventeen" ),
		"all_items" => __( "Team", "twentyseventeen" ),
		'add_new' => __( 'Nieuw werknemer', 'twentythirteen' ),
		'add_new_item' => __( 'Nieuw werknemer', 'twentythirteen' ),
	);

	$args = array(
		"label" => __( "Team", "twentyseventeen" ),
		"labels" => $labels,
		"description" => "",
		"public" => false,
		"publicly_queryable" => flase,
		"show_ui" => true,
		"show_in_rest" => false,
		"rest_base" => "",
		"has_archive" => false,
		"show_in_menu" => true,
		"exclude_from_search" => false,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"rewrite" => array( "slug" => "team", "with_front" => true ),
		"query_var" => false,
		"supports" => array( "title", "editor", "thumbnail" ),
	);

	register_post_type( "team", $args );
}

add_action( 'init', 'cptui_register_my_cpts_team' );
