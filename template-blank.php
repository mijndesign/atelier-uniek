<?php /* Template Name: Blank */ ?>

<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	
<link href="//cdn-images.mailchimp.com/embedcode/slim-10_7.css" rel="stylesheet" type="text/css">
<style type="text/css">
	#mc_embed_signup{background:#FFF8EE; clear:left; font:14px Helvetica,Arial,sans-serif; }
	/* Add your own MailChimp form style overrides in your site stylesheet or in this style block.
	   We recommend moving this block and the preceding CSS link to the HEAD of your HTML file. */
</style>
	<?php wp_head(); ?>
	<script id="mcjs">!function(c,h,i,m,p){m=c.createElement(h),p=c.getElementsByTagName(h)[0],m.async=1,m.src=i,p.parentNode.insertBefore(m,p)}(document,"script","https://chimpstatic.com/mcjs-connected/js/users/532d8fa2fcac7ee6ba76debe4/c4eb595c97c262d508aa28d46.js");</script>
	
	
</head>

<body <?php body_class(); ?>>
<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'mdtheme' ); ?></a>

	

	<div id="content" class="site-content blk">

	<div id="primary" class="content-area container ">
		<main id="main" class="site-main">
			<div class="row">
				<div class="col-sm-12 text-center p-3">
					<img src="<?php echo get_template_directory_uri(); ?>/assets/img/logo.svg" height="200px" width="100">


				</div>
			</div>
			<div class="row">
				<div class="col-md-6">
					<div class="card p-2 card-blk">
					<h2 class="lead">Atelier Uniek</h2>
					<h4>Website wordt aan gewerkt. Houd de website of onze social media kanalen in de gaten voor updates!</h4>
					</br>
					
					
					<!-- Begin MailChimp Signup Form -->

<div id="mc_embed_signup">
<form action="https://atelier-uniek.us17.list-manage.com/subscribe/post?u=532d8fa2fcac7ee6ba76debe4&amp;id=0106abfaec" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
    <div id="mc_embed_signup_scroll">
	<label for="mce-EMAIL">Blijf op de hoogte en meld je aan voor de nieuwsbrief</label>
	<input type="email" value="" name="EMAIL" class="email" id="mce-EMAIL" placeholder="email adres" required>
    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
    <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_532d8fa2fcac7ee6ba76debe4_0106abfaec" tabindex="-1" value=""></div>
    <div class="clear"><input type="submit" value="Aanmelden" name="subscribe" id="mc-embedded-subscribe" class="button"></div>
    </div>
</form>
</div>

<!--End mc_embed_signup-->
					</br>
					<p>Mocht u vragen hebben dan kunt u via de mail contact met ons opnemen</p>
					<a href="mailto:info@atelier-uniek.nl">info@atelier-uniek.nl</a>
				</div>
				<iframe src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2FAtelier-Uniek-2083519835215780%2F&width=450&layout=standard&action=like&size=small&show_faces=true&share=false&height=80&appId=786490408217603" width="450" height="80" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true"></iframe>
				</div>
				<div class="col-md-6">
					<div class="card p-2">
						<h2 class="lead">Aanmelden</h2>
						<?php echo do_shortcode( '[contact-form-7 id="9" title="Blank"]' ); ?>
						 <p class="card-text"><small class="text-muted">Uw aanmelding is pas definitief als u van ons een bevestiginsmail heeft ontvangen</small></p>
					</div>
				</div>
			</div>
			



			

		</main><!-- #main -->
	</div><!-- #primary -->

</div><!-- #content -->

	<footer id="colophon" class="site-footer">
		
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
