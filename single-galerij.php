<?php
get_header(); ?>
<?php get_template_part( 'template-parts/content', 'callout-2' ); ?>
<div id="primary" class="content-area container">
<main id="main" class="site-main row">
<div class="col-12">
<div class="card  pl-4 pr-4 pt-4 pb-4 border-0 z-depth-1">
<?php
while ( have_posts() ) : the_post();
?>
<?php 
$images = get_field('galerij');
if( $images ): ?>
<div id="slider" class="flexslider mb-0 ">
<ul class="slides">
<?php foreach( $images as $image ): ?>
<li>
<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
<p><?php echo $image['caption']; ?></p>
</li>
<?php endforeach; ?>
</ul>
</div>
<div id="carousel" class="flexslider mb-0">
<ul class="slides">
<?php foreach( $images as $image ): ?>
<li>
<img src="<?php echo $image['sizes']['thumbnail']; ?>" alt="<?php echo $image['alt']; ?>" />
</li>
<?php endforeach; ?>
</ul>
</div>
<?php endif; ?>
<div class="col-12 mt-4 d-none <?php the_field('blog_link'); ?>"><p class="bbb"><a href="<?php the_field('zoek_blog'); ?>">Lees blog</a></p></div>
</div>
<?php
the_post_navigation(array(
'prev_text'                  => __( 'vorige bericht' ),
'next_text'                  => __( 'volgende bericht' ),
));
endwhile; 
?>	
</div>
</main>
</div>
<?php
get_footer();