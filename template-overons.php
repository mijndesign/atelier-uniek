<?php
/* Template Name: Over Ons */
get_header(); ?>
<?php get_template_part( 'template-parts/content', 'callout-2' ); ?>
<div id="primary" class="content-area container">
<div class="row">
<main id="main" class="site-main col-sm-12 col-md-8 ">
<?php get_template_part( 'template-parts/page-4/content', 'row-2' ); ?>
<?php get_template_part( 'template-parts/page-4/content', 'row-4' ); ?>
<?php get_template_part( 'template-parts/page-4/content', 'row-5' ); ?>
</main>
<div class="col-sm-12 col-md-4">
<div class="card p-4 border-0 z-depth-1">
<h2 class="mb-3">Blog</h2>
<ul class="recent-post pl-0 ml-0 mb-0 ">
<?php
$recent_posts = wp_get_recent_posts(array());
foreach( $recent_posts as $recent ){
echo '<li class="mb-2 big"><a href="' . get_permalink($recent["ID"]) . '" title="Look '.esc_attr($recent["post_title"]).'" >' .   $recent["post_title"].'</a> </li> ';
}
?>							
</ul>
</div>
</div>
</div>
</div>
<?php
get_footer();