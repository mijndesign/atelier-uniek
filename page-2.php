<?php
/* Template Name: Page-2 */
get_header(); ?>
<div id="primary" class="content-area">
<main id="main" class="site-main">
<?php get_template_part( 'template-parts/home', 'content-row-1' ); ?>
<?php get_template_part( 'template-parts/home', 'content-row-2' ); ?>
<?php get_template_part( 'template-parts/home', 'content-row-3' ); ?>
<?php get_template_part( 'template-parts/home', 'content-row-4' ); ?>
<?php get_template_part( 'template-parts/home', 'content-row-5' ); ?>
</main>
</div>
<?php
get_footer();