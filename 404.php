<?php
get_header(); ?>
<?php get_template_part( 'template-parts/content', 'callout-2' ); ?>
<div id="primary" class="content-area">
<main id="main" class="site-main container">
<section class="row error-404 not-found">
<div class="col-sm-12">
<div class="card p-4 border-0 z-depth-1 text-center">
<header class="page-header">
<h1 class="page-title"><?php esc_html_e( 'Oops! Geen cursussen en workshops die de creativiteit stimuleren gevonden', 'mdtheme' ); ?></h1>
</header>
<div class="page-content text-center">
<p><?php esc_html_e( 'Niks gevonden hier, ga snel naar...', 'mdtheme' ); ?></p>
<a class="btn btn-primary m-2" href="<?php echo site_url(); ?>">Home</a>
<a class="btn btn-primary m-2" href="<?php echo site_url(); ?>/agenda">Agenda</a>
<a class="btn btn-primary m-2" href="<?php echo site_url(); ?>/activiteit">Activiteiten</a>
<a class="btn btn-primary m-2" href="<?php echo site_url(); ?>/over-ons">Over ons</a>
<a class="btn btn-primary m-2" href="<?php echo site_url(); ?>/contact">Contact</a>
</div>
</div>
</div>
</section>
</main>
</div>
<?php
get_footer();